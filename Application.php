<?php

/**
 * @brief       Application Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog;

/**
 * Polliwog Application Class
 */
class _Application extends \IPS\Application
{
    
    public function acpMenu()
    {
        if( !isset( \IPS\Data\Store::i()->polliwog_files ) )
        {
            \IPS\polliwog\Store::i()->fileStorage();
        }
        
        return parent::acpMenu();
    }
}