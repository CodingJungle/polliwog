<?php

/**
 * @brief       Purchases Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog\modules\admin\dashboard;

/* To prevent PHP errors (extending class does not exist) revealing path */
if( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

/**
 * purchases
 */
class _purchases extends \IPS\Dispatcher\Controller
{
    
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission( 'purchases_manage' );
        parent::execute();
    }
    
    /**
     * Manage
     *
     * @return    void
     */
    protected function manage()
    {
        \IPS\Output::i()->sidebar['actions']['clear'] = array(
            'icon'		=> 'eraser',
            'title'		=> 'Clear',
            'link'		=> \IPS\Http\Url::internal( 'app=polliwog&module=dashboard&controller=purchases&do=clear' )
        );

        $where = null;
        $url = 'app=polliwog&module=dashboard&controller=purchases';
        if( isset( \IPS\Request::i()->file ) ){
            $where[] = [
                'purchases_file = ?',
                (int) \IPS\Request::i()->file
            ];
            $url .= '&file='.(int) \IPS\Request::i()->file;
        }

        /* Create the table */
        $table = new \IPS\Helpers\Table\Db(
            'polliwog_purchases',
            \IPS\Http\Url::internal( $url ),
            $where
        );
        
        $table->langPrefix = 'polliwog_';

        /* Column stuff */
        $table->include = [ 'purchases_action', 'purchases_file', 'purchases_email', 'purchases_date' ];
        $table->mainColumn = 'purchases_email';
        
        /* Sort stuff */
        $table->sortBy = $table->sortBy ?: 'purchases_date';
        $table->sortDirection = $table->sortDirection ?: 'desc';
        
        /* Search */
        $table->quickSearch = 'purchases_email';
        
        $files = \IPS\Data\Store::i()->polliwog_files;
        
        /* Formatters */
        $table->parsers = [
            'purchases_date'   => function( $val, $row )
            {
                $date = \IPS\DateTime::ts( $val );
                
                return $date->localeDate() . ' ' . $date->localeTime( false );
            },
            'purchases_action' => function( $val, $row )
            {
                if( $val === "purchase" )
                {
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_purchase" );
                }
                elseif( $val === "expire" )
                {
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_expired" );
                }
                else
                {
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_renewed" );
                }
            },
            'purchases_file'   => function( $val, $row ) use ( $files )
            {
                if( isset( $files[ 'files' ][ $val ] ) )
                {
                    return $files[ 'files' ][ $val ][ 'name' ];
                }
                else
                {
                    return $val;
                }
            }
        ];

        $table->filters = array(
            'Purchases'        => 'purchases_action = \'purchase\'',
            'Expired'			=> 'purchases_action = \'expire\'',
            'Renewed'			=> 'purchases_action = \'renew\'',
        );

        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->get( 'menu__polliwog_dashboard_purchases' );

        $table->rootButtons['add'] = [
            'icon' => 'file',
            'title' => 'Files',
            'link' => 'filesMenu_button_menu',
            'id' => 'filesMenu',
            'data' => [
                'ipsMenu' => 1,
            ]
        ];

        $extra = '<ul id="filesMenu_button_menu"  class="ipsMenu ipsHide ipsMenu_narrow ipsClearfix">';
        $extra .= '<li class="ipsMenu_item"><a href="'.$this->url.'">All</a></li>';
        foreach( \IPS\polliwog\Files::roots() as $file ){
            $url = $this->url->setQueryString(['file' => $file->fid ] );
            $extra .= <<<EOF
            <li class="ipsMenu_item">
            <a href="{$url}">{$file->_title}</a>
            </li>
EOF;

        }

        $extra .= "</ul>";
        /* Display */
        \IPS\Output::i()->output = \IPS\Theme::i()->getTemplate( 'global', 'core' )->block( 'title', (string) $table ).$extra;
    }

    protected function clear(){
        \IPS\Db::i()->query( 'TRUNCATE TABLE `polliwog_purchases`');
        \IPS\Output::i()->redirect( $this->url, 'Table Truncated');
    }
}