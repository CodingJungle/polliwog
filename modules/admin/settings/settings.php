<?php

/**
 * @brief       Settings Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog\modules\admin\settings;

/* To prevent PHP errors (extending class does not exist) revealing path */
if( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

/**
 * settings
 */
class _settings extends \IPS\Dispatcher\Controller
{
    
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission( 'settings_manage' );
        parent::execute();
    }
    
    /**
     * ...
     *
     * @return    void
     */
    protected function manage()
    {

        \IPS\Member::loggedIn()->language()->words[ 'form_key_value_key' ] = \IPS\Member::loggedIn()->language()->addToStack( 'polliwog_settings_form_key_value_key' );
        
        \IPS\Member::loggedIn()->language()->words[ 'form_key_value_value' ] = \IPS\Member::loggedIn()->language()->addToStack( 'polliwog_settings_form_key_value_value' );
        
        $keys = \IPS\Settings::i()->polliwog_settings_keys ? json_decode( \IPS\Settings::i()->polliwog_settings_keys, true ) : null;
        $el['langPrefix'] = 'polliwog_settings_';
        $el[] = [
            'type' => 'dummy',
            'name' => 'push_url',
            'default' => \IPS\Http\Url::internal( 'applications/polliwog/interface/rest/rest.php?type=push', 'none' ),
            'header' => 'general',
            'tab' => 'general'
        ];

        $el[] = [
            'name' => 'keys',
            'class' => 'Stack',
            'default' => $keys,
            'required' => true,
            'options' => [
                'stackFieldType' => 'KeyValue'
            ]
        ];

        $el[] = [
            'class' => 'YesNo',
            'header' => 'members_create',
            'name' => 'sync_members',
            'default' => \IPS\Settings::i()->polliwog_settings_sync_members ?: null
        ];

        $el[] = [
            'class' => 'YesNo',
            'name' => 'create_members',
            'default' => \IPS\Settings::i()->polliwog_settings_create_members ?: null,
            'options' => [
                'togglesOn' => [ 'groups', 'mgroups' ]
            ]
        ];

        foreach( \IPS\Member\Group::groups() as $k => $v )
        {
            $groups[ $k ] = $v->name;
        }

        $el[] = [
            'class' => 'Select',
            'name' => 'groups',
            'required' => true,
            'default' => ( isset( \IPS\Settings::i()->polliwog_settings_groups ) ) ? \IPS\Settings::i()->polliwog_settings_groups : '',
            'options' => [ 'options' => $groups ]
        ];

        $el[] = [
            'class' => 'Select',
            'name' => 'mgroups',
            'default' => ( isset( \IPS\Settings::i()->polliwog_settings_mgroups ) ) ? \IPS\Settings::i()->polliwog_settings_mgroups : '',
            'options' => [
                'options' => $groups,
                'multiple' => true
            ]
        ];

        if( \IPS\Application::appIsEnabled( 'forums', true ) )
        {

            $el[] = [
                'class' => 'YesNo',
                'name' => 'post',
                'default' =>\IPS\Settings::i()->polliwog_settings_post ?: null,
                'options' => [ 'togglesOn' => [ 'member', 'forum' ] ],
                'header' => 'forums'
            ];

            $el[] = [
                'class' => 'Member',
                'name' => 'member',
                'required' => true,
                'default' => ( \IPS\Settings::i()->polliwog_settings_member ) ? \IPS\Member::load( \IPS\Settings::i()->polliwog_settings_member ) : null
            ];

            $el[] = [
                'class' => 'Node',
                'name' => 'forum',
                'required' => true,
                'default' => ( isset( \IPS\Settings::i()->polliwog_settings_forum ) ) ? \IPS\Settings::i()->polliwog_settings_forum : '',
                'options' => [
                    'class' => '\IPS\forums\Forum'
                ]
            ];

        }

//        $el[] = [
//            'class' => 'Matrix',
//
//        ]

        $form = \IPS\polliwog\Forms::i($el);

        if( $form->values() )
        {
            $values = $form->values();
            
            $values[ 'polliwog_settings_keys' ] = json_encode( $values[ 'polliwog_settings_keys' ] );
            $values[ 'polliwog_settings_member' ] = $values[ 'polliwog_settings_member' ]->member_id;
            $values[ 'polliwog_settings_forum' ] = $values[ 'polliwog_settings_forum' ]->id;
            $form->saveAsSettings( $values );
            \IPS\Output::i()->redirect( \IPS\Http\Url::internal( "app=polliwog&module=settings&controller=settings" ) );
        }
        
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->addToStack( 'settings' );
        \IPS\Output::i()->output = $form;
    }
    
    // Create new methods with the same name as the 'do' parameter which should execute it
}