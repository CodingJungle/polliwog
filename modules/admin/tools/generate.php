<?php

/**
 * @brief       Generate Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog\modules\admin\tools;

/* To prevent PHP errors (extending class does not exist) revealing path */
if( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

/**
 * generate
 */
class _generate extends \IPS\Dispatcher\Controller
{
    
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission( 'generate_manage' );
        parent::execute();
    }
    
    /**
     * ...
     *
     * @return    void
     */
    protected function manage()
    {
        $keys = json_decode( \IPS\Settings::i()->polliwog_settings_keys, true ) ?: [];
        
        $keyed[] = \IPS\Member::loggedIn()->language()->get( "polliwog_api_choose_login" );
        
        if( is_array( $keys ) && count( $keys ) )
        {
            foreach( $keys as $k => $v )
            {
                $keyed[ $v[ 'key' ] . ':' . $v[ 'value' ] ] = $v[ 'value' ];
            }
        }
        
        $el['langPrefix'] = 'polliwog_api_';
        $el[] = [
            'class' => 'Select',
            'name' => 'login',
            'required' => true,
            'options' => [ 'options' => $keyed ]
        ];

        $f[ 0 ] = \IPS\Member::loggedIn()->language()->get( "polliwog_choose_file" );
        
        $files = [];
        
        if( isset( \IPS\Data\Store::i()->polliwog_files ) )
        {
            $files = \IPS\Data\Store::i()->polliwog_files;
            if( isset( $files[ 'files' ] ) )
            {
                $files = $files[ 'files' ];
            }
        }
        
        if( is_array( $files ) && count( $files ) )
        {
            foreach( $files as $k => $v )
            {
                $f[ $k ] = $v[ 'name' ];
            }
        }

        $el[] = [
            'name' => 'file',
            'class' => 'Select',
            'required' => true,
            'options' => [ 'options' => $f ]
        ];

        $el[] = [
            'class' => 'Email',
            'name' => 'email',
            'required' => true,
            'options' => ['placeholder' => 'email@example.com' ]
        ];

        $el[] = [
            'class' => 'Date',
            'name' => 'purchase',
            'default' => time(),
            'options' => [
                'unlimited' => true,
                'unlimitedLang' => 'polliwog_skip'
            ]
        ];

        $el[] = [
            'class' => 'Date',
            'name' => 'expiration',
            'default' => time(),
            'options' => [
                'unlimited' => true,
                'unlimitedLang' => 'polliwog_skip'
            ]
        ];

        $form = \IPS\polliwog\Forms::i($el);

        if( $form->values() )
        {
            $vals = $form->values();
            
            $return = \IPS\polliwog\Generate::i()->generate(
                $vals[ 'polliwog_api_login' ],
                $vals[ 'polliwog_api_file' ],
                $vals[ 'polliwog_api_email' ],
                ( $vals[ 'polliwog_api_purchase' ] instanceof \IPS\DateTime ) ? $vals[ 'polliwog_api_purchase' ]->getTimestamp() : 0,
                ( $vals[ 'polliwog_api_expiration' ] instanceof \IPS\DateTime ) ? $vals[ 'polliwog_api_expiration' ]->getTimestamp() : 0
            );
            
            $form->addMessage( $return );
        }


        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->get( 'polliwog_tools_generate' );
        \IPS\Output::i()->output = $form;
    }
    
    // Create new methods with the same name as the 'do' parameter which should execute it
}