<?php

/**
 * @brief       Files Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog\modules\admin\tools;

/* To prevent PHP errors (extending class does not exist) revealing path */
if( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

/**
 * files
 */
class _files extends \IPS\Node\Controller
{
    
    /**
     * Node Class
     */
    protected $nodeClass = '\IPS\polliwog\Files';
    
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission( 'files_manage' );
        parent::execute();
    }

    public function links()
    {
        $id = \IPS\Request::i()->id;

        //news
        $news = \IPS\Http\Url::internal( 'applications/polliwog/interface/rest/rest.php', 'none' )->setQueryString(['type' => 'news', 'key' => $id ]);
        
        //version
        $version = \IPS\Http\Url::internal( 'applications/polliwog/interface/rest/rest.php?type=version&key=' . $id, 'none' )->setQueryString(['type' => 'version', 'key' => $id ]);
        
        $html = <<<EOF
        <div class="ipsPad">
            <div class="ipsPad ipsType_bold">Version URL: {$version}</div>
            <div class="ipsPad ipsType_bold">News URL: {$news}</div>
        </div>
EOF;
        
        \IPS\Output::i()->output .= $html;
    }
}