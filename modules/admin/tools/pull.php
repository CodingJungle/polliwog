<?php

/**
 * @brief       Pull Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       2.0.0
 * @version     -storm_version-
 */

namespace IPS\polliwog\modules\admin\tools;

/* To prevent PHP errors (extending class does not exist) revealing path */
if( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

/**
 * pull
 */
class _pull extends \IPS\Dispatcher\Controller
{
    
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission( 'pull_manage' );
        parent::execute();
    }
    
    /**
     * ...
     *
     * @return    void
     */
    protected function manage()
    {
        $keys = \IPS\Settings::i()->polliwog_settings_keys
            ? json_decode( \IPS\Settings::i()->polliwog_settings_keys, true )
            : [];
        
        $keyed[] = \IPS\Member::loggedIn()->language()->get( "polliwog_api_choose_login" );
        
        if( is_array( $keys ) && count( $keys ) )
        {
            foreach( $keys as $k => $v )
            {
                $keyed[ $v[ 'key' ] . ':' . $v[ 'value' ] ] = $v[ 'value' ];
            }
        }
        
        $files = [];
        
        if( isset( \IPS\Data\Store::i()->polliwog_files ) )
        {
            $files = \IPS\Data\Store::i()->polliwog_files;
        }
        
        if( isset( $files[ 'files' ] ) )
        {
            $files = $files[ 'files' ];
        }

        $el['langPrefix'] = 'polliwog_pull_';
        $el[] = [
            'name' => 'login',
            'class' => 'Select',
            'required' => true,
            'options' => [ 'options' => $keyed ]
        ];

        $f[ 0 ] = \IPS\Member::loggedIn()->language()->get( "polliwog_choose_file" );
        
        if( is_array( $files ) && count( $files ) )
        {
            foreach( $files as $k => $v )
            {
                $f[ $k ] = $v[ 'name' ];
            }
        }

        $el[] = [
            'name' => 'file',
            'class' => 'Select',
            'required' => true,
            'options' => [ 'options' => $f ]
        ];

        $el[] = [
            'name' => 'create',
            'class' => 'YesNo',
        ];

        $form = \IPS\polliwog\Forms::i($el);
        
        if( $form->values() )
        {
            $vals = $form->values();
            $return = \IPS\polliwog\Pull::i()->pull(
                [
                    'login'  => $vals[ 'polliwog_pull_login' ],
                    'file'   => $vals[ 'polliwog_pull_file' ],
                    'create' => $vals[ 'polliwog_pull_create' ]
                ]
            );
            $form->addMessage( $return );
            
        }
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->get( 'menu__polliwog_pull' );
        \IPS\Output::i()->output = $form;
    }
}