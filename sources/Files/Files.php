<?php

/**
 * @brief       Files Node
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

/**
 * Category Node
 */
class _Files extends \IPS\Node\Model
{
    
    /**
     * @brief    [ActiveRecord] Multiton Store
     */
    protected static $multitons;
    
    /**
     * @brief    [ActiveRecord] Default Values
     */
    protected static $defaultValues = null;
    
    /**
     * @brief    [ActiveRecord] Database Table
     */
    public static $databaseTable = 'polliwog_files';
    
    /**
     * @brief    [ActiveRecord] Database Prefix
     */
    public static $databasePrefix = 'files_';
    
    /**
     * @brief    [Node] Order Database Column
     */
    public static $databaseColumnOrder = 'id';

    /**
     * @brief    [ActiveRecord] Database ID Fields
     */
    protected static $databaseIdFields = ['id', 'files_fnexus_id'];

    /**
     * @brief    [Node] Parent ID Database Column
     */
    
    /**
     * @brief    [Node] Node Title
     */
    public static $nodeTitle = 'polliwog_files';
    
    /**
     * @brief    [Node] ACP Restrictions
     * @code
    array(
     * 'app'        => 'core',                // The application key which holds the restrictrions
     * 'module'    => 'foo',                // The module key which holds the restrictions
     * 'map'        => array(                // [Optional] The key for each restriction - can alternatively use "prefix"
     * 'add'            => 'foo_add',
     * 'edit'            => 'foo_edit',
     * 'permissions'    => 'foo_perms',
     * 'delete'        => 'foo_delete'
     * ),
     * 'all'        => 'foo_manage',        // [Optional] The key to use for any restriction not provided in the map (only needed if not providing all 4)
     * 'prefix'    => 'foo_',                // [Optional] Rather than specifying each  key in the map, you can specify a prefix, and it will automatically look for restrictions with the key "[prefix]_add/edit/permissions/delete"
     * @encode
     */
    protected static $restrictions = [
        'app'    => 'polliwog',
        'module' => 'files',
        'prefix' => 'files_'
    ];
    
    /**
     * @brief    [Node] Title search prefix.  If specified, searches for '_title' will be done against the language pack.
     */
    public static $titleSearchPrefix = 'polliwog_files_';
    
    /**
     * [Node] Get title
     *
     * @return    string
     */
    protected function get__title()
    {
        $cache = [];
        if( isset( \IPS\Data\Store::i()->polliwog_files ) )
        {
            $cache = \IPS\Data\Store::i()->polliwog_files;
        }
        if( isset( $cache[ 'files' ][ $this->fid ] ) )
        {
            $file = $cache[ 'files' ][ $this->fid ];
            if( isset( $file[ 'name' ] ) )
            {
                return $file[ 'name' ];
            }
        }
        
        return $this->ftitle;
    }
    
    /**
     * [Node] Get whether or not this node is enabled
     *
     * @note    Return value NULL indicates the node cannot be enabled/disabled
     * @return    bool|null
     */
    protected function get__enabled()
    {
        return $this->open;
    }
    
    /**
     * [Node] Set whether or not this node is enabled
     *
     * @param    bool|int $enabled Whether to set it enabled or disabled
     * @return    void
     */
    protected function set__enabled( $enabled )
    {
        $this->open = $enabled;
    }
    
    /**
     * [Node] Add/Edit Form
     *
     * @param    \IPS\Helpers\Form $form The form
     * @return    void
     */
    public function form( &$form )
    {
        $el['langPrefix'] = 'polliwog_files_';
        if( \IPS\Application::appIsEnabled( 'downloads', true ) )
        {
            $f[ 0 ] = \IPS\Member::loggedIn()->language()->get( "polliwog_files_choose" );
            $sql = \IPS\Db::i()->select( '*', 'downloads_files' );

            foreach( $sql as $k => $v )
            {
                $f[ $v[ 'file_id' ] ] = $v[ 'file_name' ];
            }

            $el[] = [
                'name' => 'fnexus_id',
                'class' => 'Select',
                'default' => $this->fnexus_id ?: null,
                'tab' => 'general',
                'options' => [
                    'options' => $f,
                    'toggles' => [ [ 0 => 'ftitle' ] ]
                ]
            ];
        }

        $el[] = [
            'name' => 'ftitle',
            'default' => $this->ftitle,
            'appearRequired' => true,
            'tab' => 'general',
            'v' =>  function( $val )
            {
                if( !intval( \IPS\Request::i()->polliwog_files_fnexus_id ) )
                {
                    if( !$val )
                    {
                        throw new \InvalidArgumentException( 'polliwog_files_name_blank' );
                    }
                }
            }
        ];

        $el[] = [
            'class' => 'Number',
            'name' => 'fid',
            'default' => $this->fid?:0,
            'required' => true,
            'options' => [ 'step' => 1],
            'tab' => 'general',
        ];

        \IPS\Member::loggedIn()->language()->words[ 'form_key_value_key' ] = \IPS\Member::loggedIn()->language()->get( 'polliwog_files_news_title' );
        
        \IPS\Member::loggedIn()->language()->words[ 'form_key_value_value' ] = \IPS\Member::loggedIn()->language()->get( 'polliwog_files_news_url' );
        
        $news = json_decode( $this->news, true );
        $el[] = [
            'class' => 'Stack',
            'name' => 'news',
            'tab' => 'news',
            'default' => $news ?: '',
            'options' => [
                'key'            => [ 'placeholder' => "Title" ],
                'value'          => [ 'placeholder' => "http://example.com" ],
                'stackFieldType' => 'KeyValue'
            ]
        ];

        

        $el[] = [
            'name' => 'short',
            'default' => $this->short ?: '',
            'options' => [ 'placeholder' => '1.0.0' ],
            'tab' => 'versions'
        ];

        $el[] = [
            'name' => 'long',
            'class' => 'Number',
            'default' => $this->long ?: null,
            'options' => [ 'placeholder' => 100000 ],
            'tab' => 'versions'
        ];

        $el[] = [
            'class' => 'Date',
            'name' => 'released',
            'default' => $this->released ?: 0,
            'tab' => 'versions'
        ];

        $el[] = [
            'name' => 'updateurl',
            'default' => $this->updateurl ?: '',
            'options' => [ 'placeholder' => 'http://example.com' ],
            'tab' => 'versions'
        ];

        $form = \IPS\polliwog\Forms::i($el, null, 'default', $form);
    }
    
    /**
     * [Node] Format form values from add/edit form for save
     *
     * @param    array $values Values from the form
     * @return    array
     */
    public function formatFormValues( $values )
    {
        foreach( $values as $k => $v )
        {
            $new[ str_replace( "polliwog_files_", "", $k ) ] = $v;
        }
        
        return $new;
    }
    
    public function getButtons( $url, $subnode = false )
    {
        $buttons = parent::getButtons( $url, $subnode );
        $buttons[ 'links' ] = [
            'icon'  => 'link',
            'title' => 'links',
            'link'  => \IPS\Http\Url::internal( "app=polliwog&module=tools&controller=files&id={$this->_id}&do=links" ),
            'data'  => [ 'ipsDialog' => '', 'ipsDialog-forceReload' => 'true', 'ipsDialog-title' => \IPS\Member::loggedIn()->language()->addToStack( 'polliwog_links_acp' ) ]
        ];
        
        return $buttons;
    }
    
    /**
     * [Node] Save Add/Edit Form
     *
     * @param    array $values Values from the form
     * @return    void
     */
    public function saveForm( $values )
    {
        if( !$this->id )
        {
            $this->save();
        }
        
        $values[ 'fnexus_id' ] = ( intval( $values[ 'fnexus_id' ] ) ) ? $values[ 'fnexus_id' ] : 0;
        $values[ 'news' ] = json_encode( $values[ 'news' ] );
        if( $values[ 'released' ] instanceof \IPS\DateTime )
        {
            $values[ 'released' ] = $values[ 'released' ]->getTimestamp();
        }
        parent::saveForm( $values );
        
        \IPS\polliwog\Store::i()->fileStorage();
    }
    
    public function canCopy()
    {
        return false;
    }
    
    /**
     * Delete menus
     */
    public function delete()
    {
        parent::delete();
        \IPS\polliwog\Store::i()->fileStorage();
    }
}