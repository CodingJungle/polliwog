<?php

/**
 * @brief       Member Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Member extends \IPS\Patterns\Singleton
{
    
    /**
     * @brief    Singleton Instances
     * @note     This needs to be declared in any child classes as well, only declaring here for editor
     *           code-complete/error-check functionality
     */
    protected static $instance = null;
    
    public function create( $data, $email = 1 )
    {
        $password = $this->_randPass();
        $member = new \IPS\Member;
        $member->name = $this->_memberName( $data->email );
        $member->email = $data->email;
        $member->member_group_id = \IPS\Settings::i()->polliwog_settings_groups;
        $member->mgroup_others = \IPS\Settings::i()->polliwog_settings_mgroups ?: '';
        $member->language = \IPS\Lang::defaultLanguage();
        $member->skin = 0;
        $member->members_pass_salt = $member->generateSalt();
        $member->members_pass_hash = $member->encryptedPassword( $password );
        $member->members_bitoptions[ 'coppa_user' ] = false;
        $member->save();
        
        /* Reset statistics */
        \IPS\Widget::deleteCaches( 'stats', 'core' );
        
        if( intval( $email ) )
        {
            \IPS\Email::buildFromTemplate( 'core', 'admin_reg', [ $member, $password ] )->send( $member );
        }
        
        return \IPS\Member::load( $data->email, 'email' );
    }
    
    protected function _memberName( $name )
    {
        $name = preg_replace( '/\@([^\n]+)?/i', '', $name ) . rand( 0, 1000 );
        
        return $name;
    }
    
    protected function _randPass()
    {
        $rand = [ "]", "[", "{", "}", ";", ":", "&", "!", "@", "#", "%", "+", "*", "(", "_" ];
        $d = '';
        for( $x = 0; $x < 10; $x++ )
        {
            $r = rand( 0, 1000 );
            $r2 = rand( 0, 1000 );
            if( $r % 2 == 0 )
            {
                $d .= chr( mt_rand( ord( 'A' ), ord( 'Z' ) ) );
            }
            else
            {
                if( $r2 % 2 == 0 )
                {
                    $d .= chr( mt_rand( ord( 'a' ), ord( 'z' ) ) );
                }
                else
                {
                    $d .= rand( 0, 9 );
                }
            }
            $d = $d . " ";
        }
        $d = explode( " ", trim( $d ) );
        for( $x = 0; $x < 5; $x++ )
        {
            $r = rand( 0, 9 );
            $r2 = rand( 0, 14 );
            $d[ $r ] = $d[ $r ] . $rand[ $r2 ];
        }
        
        return implode( "", $d );
    }
}