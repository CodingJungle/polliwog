<?php

/**
 * @brief       Generate Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Generate extends \IPS\Patterns\Singleton
{
    
    /**
     * @brief    Singleton Instances
     * @note     This needs to be declared in any child classes as well, only declaring here for editor
     *           code-complete/error-check functionality
     */
    protected static $instance = null;
    
    public function generate( $login, $file, $email, $purchase = 0, $expiration = 0 )
    {
        
        try
        {
            list( $key, $login ) = explode( ":", $login );
            
            $post[ 'file' ] = $file;
            
            $post[ 'email' ] = trim( $email );
            
            if( intval( $purchase ) )
            {
                $post[ 'purchased' ] = $purchase;
            }
            
            if( intval( $expiration ) )
            {
                $post[ 'expire' ] = $expiration;
            }
            
            $response = \IPS\Http\Url::external( 'https://community.invisionpower.com/applications/contributorcenter/interface/api/generate.php' )
                ->request()
                ->login( trim( $login ), trim( $key ) )
                ->post(
                    $post
                );
            
            if( $response->httpResponseCode == 200 )
            {
                return \IPS\Member::loggedIn()->language()->get( "polliwog_response_generated" );
            }
            else
            {
                if( $response->httpResponseCode == 401 )
                {
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_repsonse_bad_pass" );
                }
                
                if( $response->httpResponseCode == 403 )
                {
                    $content = mb_strtolower( $response->content );
                    
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_repsonse_" . $content );
                }
                
            }
        }
        catch( \IPS\Http\Request\Exception $e )
        {
            return \IPS\Member::loggedIn()->language()->get( "polliwog_generic_error" );
        }
    }
}