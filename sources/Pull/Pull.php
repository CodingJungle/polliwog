<?php

/**
 * @brief       Pull Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Pull extends \IPS\Patterns\Singleton
{
    
    /**
     * @brief    Singleton Instances
     * @note     This needs to be declared in any child classes as well, only declaring here for editor
     *           code-complete/error-check functionality
     */
    protected static $instance = null;
    
    public function pull( $data )
    {
        try
        {
            list( $key, $login ) = explode( ":", $data[ 'login' ] );
            
            $response = \IPS\Http\Url::external( 'https://community.invisionpower.com/applications/contributorcenter/interface/api/pull.php' )
                ->setQueryString( [ 'file' => $data[ 'file' ] ] )
                ->request()
                ->login( trim( $login ), trim( $key ) )
                ->setHeaders( [ 'Accept' => 'application/json' ] )
                ->get();
            
            if( $response->httpResponseCode == 200 )
            {
                $purchases = $response->decodeJson( false );
                foreach( $purchases as $p )
                {
                    $action = "purchase";
                    $exist = 0;
                    if( $p->EXPIRATION_DATE and time() > $p->EXPIRATION_DATE )
                    {
                        $action = "expire";
                    }
                    
                    try
                    {
                        $pr = \IPS\polliwog\Push::load( $p->PURCHASE_ID );
                    }
                    catch( \OutOfRangeException $e )
                    {
                        $pr = new \IPS\polliwog\Push;
                    }
                    
                    if( !isset( $pr->id ) )
                    {
                        $pr->id = $p->PURCHASE_ID;
                        $pr->date = $p->DATE_PURCHASED;
                        $pr->save();
                    }
                    
                    $pr->file = $data[ 'file' ];
                    $pr->cust = $p->CUST_ID;
                    $pr->email = $p->CUST_EMAIL;
                    $pr->action = $action;
                    
                    if( \IPS\Settings::i()->polliwog_settings_sync_members && !$pr->member_id )
                    {
                        $member = \IPS\Member::load( $pr->email, 'email' );
                        if( $member->member_id )
                        {
                            $p->member_id = $member->member_id;
                            $exist = 1;
                        }
                    }

                    if( $data['polliwog_pull_create'] )
                    {
                        if( !intval( $exist ) && \IPS\Settings::i()->polliwog_settings_create_members && $p->action === "expire" )
                        {
                            $member = \IPS\polliwog\Member::i()->create( $p );
                            if( !$pr->member_id && \IPS\Settings::i()->polliwog_settings_sync_members )
                            {
                                $pr->member_id = $member->member_id;
                            }
                        }
                    }
                    $pr->save();
                }
                
                return \IPS\Member::loggedIn()->language()->get( "polliwog_response_pull_success" );
            }
            elseif( $response->httpResponseCode == 403 )
            {
                return \IPS\Member::loggedIn()->language()->get( "polliwog_repsonse_bad_pass" );
            }
            else
            {
                if( $response->httpResponseCode == 403 )
                {
                    $content = mb_strtolower( $response->content );
                    
                    return \IPS\Member::loggedIn()->language()->get( "polliwog_repsonse_" . $content );
                }
                
                return \IPS\Member::loggedIn()->language()->get( "polliwog_generic_error" );
            }
        }
        catch( \IPS\Http\Request\Exception $e )
        {
            return \IPS\Member::loggedIn()->language()->get( "polliwog_generic_error2" );
        }
    }
}