<?php

/**
 * @brief       Push Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Push extends \IPS\Patterns\ActiveRecord
{
    
    /**
     * @brief    [ActiveRecord] Multiton Store
     */
    protected static $multitons;
    
    /**
     * @brief    [ActiveRecord] Database Table
     */
    public static $databaseTable  = 'polliwog_purchases';
    
    public static $databasePrefix = "purchases_";
    
    /**
     * @brief    [ActiveRecord] ID Database Column
     */
    public static $databaseColumnId = 'id';
    
    /**
     * @brief    [ActiveRecord] Database ID Fields
     */
    protected static $databaseIdFields = [ 'name', 'email', 'id' ];
}