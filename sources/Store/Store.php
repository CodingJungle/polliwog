<?php

/**
 * @brief       Store Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Store extends \IPS\Patterns\Singleton
{
    
    /**
     * @brief    Singleton Instances
     * @note     This needs to be declared in any child classes as well, only declaring here for editor
     *           code-complete/error-check functionality
     */
    protected static $instance = null;

    public function getFileStore(){
        if( !isset( \IPS\Data\Store::i()->polliwo_files ) ){
            $this->fileStorage();
        }

        return \IPS\Data\Store::i()->polliwog_files;
    }

    public function fileStorage()
    {
        $data = [];
        
        if( isset( \IPS\Data\Store::i()->polliwog_files ) )
        {
            unset( \IPS\Data\Store::i()->polliwog_files );
        }
        
        $files = \IPS\polliwog\Files::roots( null );
        
        foreach( $files as $k => $v )
        {
            if( \IPS\Application::appIsEnabled( 'downloads' ) )
            {
                try
                {
                    $file = \IPS\Db::i()->select( '*', 'downloads_files', [ 'file_id =?', $v->fnexus_id ] )->first();
                    $title = $file[ 'file_name' ];
                }
                catch( \UnderflowException $e )
                {
                    $file = 0;
                    $title = $v->ftitle;
                }
            }
            else
            {
                $file = 0;
                $title = $v->ftitle;
            }
            
            $data[ 'files' ][ $v->fid ] = [
                'name'  => $title,
                'nexus' => $file
            ];
            
            $data[ 'news' ][ $v->id ] = json_decode( $v->news, true );
            
            $data[ 'version' ][ $v->id ] = [
                'version'     => $v->short,
                'longversion' => $v->long,
                'released'    => $v->released,
                'updateurl'   => $v->updateurl
            ];
        }
        
        \IPS\Data\Store::i()->polliwog_files = $data;
    }
    
}