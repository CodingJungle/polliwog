<?php

/**
 * @brief       Post Class
 * @author      -storm_author-
 * @copyright   -storm_copyright-
 * @package     IPS Social Suite
 * @subpackage  Polliwog
 * @since       1.0.4
 * @version     -storm_version-
 */

namespace IPS\polliwog;

class _Post extends \IPS\Patterns\Singleton
{
    
    /**
     * @brief    Singleton Instances
     * @note     This needs to be declared in any child classes as well, only declaring here for editor
     *           code-complete/error-check functionality
     */
    protected static $instance = null;
    
    public function post( $data )
    {
        if( (int) \IPS\Settings::i()->polliwog_settings_forum and
            (int) \IPS\Settings::i()->polliwog_settings_member and
            (int)  \IPS\Settings::i()->polliwog_settings_post
        )
        {
            $store = \IPS\polliwog\Store::i()->getFileStore();
            if( $store )
            {
                $file = $store;
                if( isset( $file[ 'files' ][ $data->file ] ) )
                {
                    $file = $file[ 'files' ][ $data->file ];
                    $fname = $file[ 'name' ];
                }
                else
                {
                    $fname = $data->file;
                }
            }
            else
            {
                $fname = $data->file;
            }
            
            $find = [
                "{file}",
                "{email}",
                "{type",
                "{date}",
                "{pid}",
                "{cid}"
            ];
            
            $replacement = [
                $fname,
                $data->email,
                $data->action,
                $data->date,
                $data->purchase_id,
                $data->cust
            ];
            
            $content = str_replace(
                $find,
                $replacement,
                \IPS\Member::loggedIn()
                    ->language()
                    ->get( "polliwog_post_body_" . $data->action )
            );
            
            $title = str_replace(
                $find,
                $replacement,
                \IPS\Member::loggedIn()
                    ->language()
                    ->get( "polliwog_post_title_" . $data->action )
            );
            
            $member = \IPS\Member::load( \IPS\Settings::i()->polliwog_settings_member );
            
            $forum = \IPS\forums\Forum::load( \IPS\Settings::i()->polliwog_settings_forum );
            
            $topic = \IPS\forums\Topic::createItem(
                $member,
                $member->ip_address,
                \IPS\DateTime::ts( time() ),
                $forum
            );
            
            $topic->title = $title;
            $topic->save();
            
            $post = \IPS\forums\Topic\Post::create(
                $topic,
                $content,
                true,
                null,
                true,
                $member,
                \IPS\DateTime::ts( time() )
            );
            
            $topic->topic_firstpost = $post->pid;
            $topic->save();
        }
    }
}