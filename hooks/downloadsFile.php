//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

/**
 * Class polliwog_hook_downloadsFile \IPS\downloads\File
 */
class polliwog_hook_downloadsFile extends _HOOK_CLASS_
{

    public static function formElements( $item=NULL, \IPS\Node\Model $container=NULL )
    {
        $parent = parent::formElements( $item, $container );
        $parent['polliwog_long_version'] = new \IPS\Helpers\Form\Number( 'polliwog_long_version', ( $item ) ? $item->polliwog_long_version : null, false, array( 'maxLength' => 32 ) );

        return $parent;

    }

    public function processForm( $values )
    {
        if( isset( $values['polliwog_long_version'] ) ){
            $this->polliwog_long_version = (int) $values['polliwog_long_version'];
        }
        return parent::processForm( $values );
    }

}
