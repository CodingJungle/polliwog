//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class polliwog_hook_downloadsFrontSubmit extends _HOOK_CLASS_
{

/* !Hook Data - DO NOT REMOVE */
public static function hookData() {
 return  parent::hookData();
}
/* End Hook Data */

    /**
     * @param $form \IPS\Helpers\Form
     * @param $versioning
     * @return mixed
     */
public function newVersion($form, $versioning){

    try
    {
        $file = \IPS\downloads\File::load( (int) \IPS\Request::i()->id );
        $file = $file->polliwog_long_version;

    }catch( \Exception $e ){
        $file = null;
    }

    $polliwog = new \IPS\Helpers\Form\Number( 'polliwog_long_version', $file, false,
        array( 'maxLength' => 32 ) );

    $form->add( $polliwog , 'file_version' );
    return parent::newVersion( $form, $versioning );
}
}
