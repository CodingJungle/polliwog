//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class polliwog_hook_downloadsVIew extends _HOOK_CLASS_
{
    protected function newVersion()
    {
        if( isset( \IPS\Request::i()->polliwog_long_version ) ){
            $this->file->polliwog_long_version = (int) \IPS\Request::i()->polliwog_long_version;
        }
        parent::newVersion();
    }

}
