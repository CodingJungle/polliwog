//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class polliwog_hook_nexusSupport extends _HOOK_CLASS_
{

/* !Hook Data - DO NOT REMOVE */
public static function hookData() {
 return array_merge_recursive( array (
  'request' => 
  array (
    0 => 
    array (
      'selector' => '.cNexusSupportRequest_metaInfo',
      'type' => 'add_inside_start',
      'content' => '{template="purchases" group="support" app="polliwog" params="$request"}',
    ),
  ),
), parent::hookData() );
}
/* End Hook Data */


}
