<?php
require_once '../../../../init.php';
\IPS\Session\Front::i();
$type = \IPS\Request::i()->type;

if( $type === "generate" )
{
    $post = [
        'CUST_ID' => 63,
        'PURCHASE_ID' => 567333,
        'CUST_EMAIL' => 'codingjungle@gmail325621452.com',
        'FILE' => 1234,
        'ACTION' => "purchase",
        'KEY' => '',
    ];

    $p = \IPS\Http\Url::external( 'http://10.0.0.20/~michael/dev/applications/polliwog/interface/rest/rest.php?type=push' )
                      ->request()
                      ->post(
                          $post
                      );
    print_r( $p );
    exit;
}


if( $type === "version" )
{
    $appKey = \IPS\Request::i()->key;

    $versions = function() use ( $appKey )
    {
        $app = \IPS\Data\Store::i()->polliwog_files;

        $return = [];

        if( intval( $app ) )
        {
            if( isset( $app[ 'version' ][ $appKey ] ) )
            {
                $return = $app[ 'version' ][ $appKey ];
            }
        }

        return $return;
    };

    try
    {
        $file = \IPS\polliwog\Files::load( $appKey );

        $downloadFile = \IPS\downloads\File::load( $file->fnexus_id );

        $fields = $downloadFile->customFields();

        $return = [
            'version' => $downloadFile->version,
            'longversion' => $downloadFile->polliwog_long_version,
            'released' => $downloadFile->updated ?: $downloadFile->submitted,
            'updateurl' => $file->updateurl ?: $downloadFile->url(),
            'releasenotes' => $downloadFile->changelog ?: ''
        ];


    }
    catch( \Exception $e )
    {
        $return = $versions();
    }

    \IPS\Output::i()->json( $return );

    exit;
}

if( $type === "news" )
{
    $app = \IPS\Data\Store::i()->polliwog_files;

    $appKey = \IPS\Request::i()->key;

    $return = [];

    if( intval( $app ) )
    {
        if( isset( $app[ 'news' ][ $appKey ] ) )
        {
            $return = $app[ 'news' ][ $appKey ];
        }
    }

    \IPS\Output::i()->sendOutput( json_encode( $return ), 200, 'application/json' );
    exit;
}

if( $type === "push" )
{
    $keys = \IPS\Settings::i()->polliwog_settings_keys
        ? json_decode( \IPS\Settings::i()->polliwog_settings_keys, true )
        : [];
    $exist = 0;

    if( is_array( $keys ) and count( $keys ) )
    {
        foreach( $keys as $k => $v )
        {
            $keyed[ $v[ 'key' ] ] = $v[ 'value' ];
        }
    }

//    if( !isset( $keyed[ \IPS\Request::i()->KEY ] ) )
//    {
//        exit;
//    }

    try
    {
        $p = \IPS\polliwog\Push::load( (int) \IPS\Request::i()->PURCHASE_ID );
    }
    catch( \Exception $e )
    {
        $p = new \IPS\polliwog\Push;
        $p->id = (int)\IPS\Request::i()->PURCHASE_ID;
        $p->date = time();
        $p->save();
    }

    switch( \IPS\Request::i()->ACTION )
    {
        case "purchase":
            $p->file = (int)\IPS\Request::i()->FILE;
            $p->cust = (int)\IPS\Request::i()->CUST_ID;
            $p->email = \IPS\Request::i()->CUST_EMAIL ?: '';
            $p->action = "purchase";
            break;
        case 'expire':
            $p->file = (int)\IPS\Request::i()->FILE;
            $p->action = "expire";
            break;
        case 'renew':
            $p->file = (int)\IPS\Request::i()->FILE;
            $p->cust = (int)\IPS\Request::i()->CUST_ID;
            $p->action = "renew";
            break;
    }

    $member = \IPS\Member::load( $p->email, 'email' );

    if( !$member->member_id && \IPS\Settings::i()->polliwog_settings_create_members and $p->email )
    {
        $member = \IPS\polliwog\Member::i()->create( $p );
    }

    if( \IPS\Settings::i()->polliwog_settings_sync_members )
    {
        $p->member_id = $member->member_id;
    }

    if( $member->id and !$p->email )
    {
        $p->email = $member->email;
    }

    \IPS\polliwog\Post::i()->post( $p );
    $p->save();

    echo "OKAY";
    exit;
}